﻿using BulkyBook1.Models;

namespace BulkyBook1.DataAccess.Repository.IRepository
{
    public interface ICompanyRepository : IRepository<Company>
    {
        void Update(Company company);
    }
}