﻿using BulkyBook1.DataAccess.Repository.IRepository;
using BulkyBook1.Models;

namespace BulkyBook1.DataAccess.Repository
{
    public class OrderHeaderRepository : Repository<OrderHeader>, IOrderHeaderRepository
    {
        private ApplicationDbContext _db;
        public OrderHeaderRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(OrderHeader orderHeader)
        {
            _db.OrderHeaders.Update(orderHeader);
        }

        public void UpdateStatus(int id, string orderStatus, string? paymentStatus = null)
        {
            var orderFromDb = _db.OrderHeaders.FirstOrDefault(o => o.Id == id);
            if (orderFromDb == null)
                return;
            orderFromDb.OrderStatus = orderStatus;
            orderFromDb.PaymentStatus = paymentStatus;
        }   

        public void UpdateStripePayment(int id, string sessionId, string paymentIntenId)
        {
            var orderFromDb = _db.OrderHeaders.FirstOrDefault(o => o.Id == id);
            if(orderFromDb == null)
                return ;
            orderFromDb.PaymentDate = DateTime.Now; 
            orderFromDb.SessionId = sessionId;
            orderFromDb.PaymentIntentId = paymentIntenId;
        }
    }
}