﻿var dataTable;
$(document).ready(function () {
    var url = window.location.search;
    if (url.includes("inprocess")) {
        loadDataTable("GetALL?status=inprocess");
    }
    else {
        if (url.includes("pending")) {
            loadDataTable("GetALL?status=pending");
        }
        else {
            if (url.includes("completed")) {
                loadDataTable("GetALL?status=completed");
            }
            else {
                if (url.includes("rejected")) {
                    loadDataTable("GetALL?status=rejected");
                }
                else {
                    loadDataTable("GetALL?status=all");
                }
            }
        }
    }
});
function loadDataTable(url) {
    dataTable = $('#tblData').DataTable({
        "ajax": {
            "url": "/Admin/Order/" + url
        },
        "columns": [
            { "data": "id", "width": "15%" },
            { "data": "name", "width": "15%" },
            { "data": "phoneNumber", "width": "15%" },
            { "data": "applicationUser.email", "width": "15%" },
            { "data": "orderStatus", "width": "15%" },
            { "data": "orderTotal", "width": "15%" },
            {
                "data": "id",
                "render": function (data) {
                    return `
                        <div class="w-75 btn-group" role="group">
                        <a href="/Admin/Order/Details?orderid=${data}" class="btn btn-primary mx-2" >
                            <i class="bi bi-pencil-square"></i> View
                        </a>
                        
                    </div >
                        `
                },
                "witdh": "15%"
            }
        ]
    });
}

